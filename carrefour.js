const fs = require('fs');
const readline = require('readline');
const Commande = require('./commande.class');
const pdfToTxt = require('./pdfToTxt');


function getOrderFromTxt(file){
    return new Promise((resolve, reject) => {
        let searchCustomer = true;
        let searchDateCommande = true;
        let order = null;
    
        const rl = readline.createInterface({
            input: fs.createReadStream('./newOrder/' + file, 'ascii')
        });
             
        let cpt = 0;
        let commandes = [];
        let dateCommande = "";
    
        rl.on('line', (line) => {
            if(searchCustomer && line.includes("97410")){
                if(line.includes("97441")){
                    console.log("Carrefour STE SUZANNE");
                    order = new Commande("CARREFOUR STE SUZA");
                    order.rowNavette = 21;
                    searchCustomer = false;
                }else if(line.includes("97495")){
                    console.log("Carrefour SAINTE-CLOTILDE");
                    order = new Commande("CARREFOUR ST DENIS");
                    order.rowNavette = 19;
                    searchCustomer = false;
                }else{
                    console.log("Carrefour SAINT PIERRE");
                    order = new Commande("CARREFOUR ST PIERRE");
                    order.rowNavette = 20;
                    searchCustomer = false;
                }
            }else if(!searchCustomer){
                if(line.includes("Date commande") && searchDateCommande){
                    let listMotDateCommande = [];
                    listMotDateCommande = line.split(" ");
                    dateCommande = listMotDateCommande[listMotDateCommande.length - 1];
                    dateCommande = dateCommande.split("/").join("-");
                    order.dateCommande = dateCommande;
                    searchDateCommande = false;
                }
                if(line.includes("Prix achat en EUR")){
                    // debut enregistrement dans +1
                    cpt = 2;
                }else if(line.includes("-------------------") && cpt < 2){
                    cpt = 0;
                }else if(cpt == 2){
                    cpt--;
                }else if(cpt == 1){
                    let oneCommande = line.split('&');

                    oneCommande.forEach((element, index) => {
                        oneCommande[index] = element.trim();
                    })

                    let colNavette = tabCorrespondanceLibelle(oneCommande[4]);

                    if(colNavette == "C"){
                        oneCommande[7] = (oneCommande[7].split(" ")[0] / 5);
                        oneCommande[7] = (Math.floor(oneCommande[7])).toString();
                    }else if(colNavette == "F"){
                        oneCommande[7] = oneCommande[7].split(" ")[0] / 3;
                        oneCommande[7] = (Math.floor(oneCommande[7])).toString();
                    }else if(colNavette == "G" || colNavette == "H" || colNavette == "J" || colNavette == "K"){
                        oneCommande[7] = oneCommande[5];
                    }else if(colNavette == "D"){
                        oneCommande[7] = (oneCommande[7].split(" ")[0] / 20);
                        oneCommande[7] = (Math.floor(oneCommande[7])).toString();
                    }

                    oneCommande.quantity = oneCommande[7];
                    oneCommande['colNavette'] = colNavette;
                    commandes.push(oneCommande);
                }
            }
        })
    
        rl.on('close', () => {
            if(order){
                let numCommande = file.split("_")[1];
                order.setNumCommande(numCommande);
                order.setCommande(commandes);
                resolve(order);
            }else{
                reject("Les produits n'ont pas été trouvé");
            }
        })
    });
}

function tabCorrespondanceLibelle(libelle){
    switch (libelle) {
        case 'ENDIVE':
            return 'C';
            break;

        case 'ENDIVE SACHET 500G':
            return 'D';
            break;
    
        case 'ENDIVE SACHET 750G':
            return 'E';
            break;

        case 'CHAMPIGNON PARIS':
            return 'F';
            break;
    
        case 'CHAMPIGNONS PARIS BLCS 250G':
            return 'G';
            break;

        case 'CHAMPIGNONS DE PARIS 500G':
            return 'H';
            break;

        case 'CHAMPIGNONS BRUNS':
            return 'I';
            break;

        case 'CHAMPIGNONS BRUNS 250G':
            return 'J';
            break;

        case 'CHAMPIGNONS BRUNS 500G':
            return 'K';
            break;
        default:
            break;
    }
}


function getOrderFromPdf(file){
    return new Promise(async (resolve, reject) => {
        let order = null;

        pdfToTxt.exec('./newOrder/' + file)
        .then((text) => {
            if(text.includes("(N°74)-CARREFOUR MARKET ST")){
                order = new Commande("CARREFOUR MARKET ST JOSEPH");
                order.rowNavette = 16;
            }else if(text.includes("(N°29)-Carrefour Saint-Benoît")){
                order = new Commande("CARREFOUR ST BENOIT");
                order.rowNavette = 11;
            }else if(text.includes("(N°87)-Carrefour Market Bel Air")){
                order = new Commande("CARREFOUR MARKET BEL AIR");
                order.rowNavette = 13;
            }else if(text.includes("(N°72)-Carrefour Market Vauban")){
                order = new Commande("CARREFOUR MARKET VAUBAN");
                order.rowNavette = 18;
            }else if(text.includes("(N°61)-Carrefour Grand Large")){
                order = new Commande("CARREFOUR GRAND LARGE");
                order.rowNavette = 10;
            }else if(text.includes("(N°65)-Carrefour Market Bellepierre")){
                order = new Commande("CARREFOUR MARKET BELPIERRE");
                order.rowNavette = 14;
            }else if(text.includes("(N°86)-Carrefour Market Terre Rouge")){
                order = new Commande("CARREFOUR MARKET TERRE ROUGE");
                order.rowNavette = 17;
            }else{
                reject("Client n'a pas été identifier");
            }

            order.dateCommande = text[text.indexOf('Commande') + 3].split(" ")[2].split("/");
            order.dateCommande[2] = Math.floor((order.dateCommande[2] / 100 - Math.floor(order.dateCommande[2] / 100)) * 100);
            order.dateCommande = order.dateCommande.join("-");
            
            let posReading = text.indexOf("Mt TTC") + 1;
            let commandes = [];
            
            while(text[posReading] != "Montant Total Net"){
                let oneCommande = {};
                oneCommande.ref = text[posReading];
                oneCommande.desc = text[posReading + 1];
                oneCommande.quantity = text[posReading + 2].split('(')[1].split(" ")[0];

                let colNavette = tabCorrespondanceDescription(oneCommande.desc);

                if(colNavette == -1){
                    reject("Le produit n'a pas été identitifier: " + oneCommande.desc);
                }
                oneCommande.colNavette = colNavette;
                commandes.push(oneCommande);
                posReading += 6;
            }

            let numCommande = null;
            let cpt = 0;
            while(!numCommande){
                if(text[cpt].includes("Numéro de commande")){
                    numCommande = text[cpt].split(" ")[3].toString();
                }
                cpt++;
            }
            numCommande = numCommande.substring(numCommande.length - 5, numCommande.length);

            order.setNumCommande(numCommande);
            order.setCommande(commandes);

            resolve(order);
        });
    });
}

function tabCorrespondanceDescription(description){
    if(description.includes("ENDIVES REUNION")){
        return 'C';
    }else if(description.includes("ENDIVE SACHET 500G")){
        return 'D';
    }else if(description.includes("ENDIVE SACHET 750G")){
        return 'E';
    }else if(description.includes("CHAMPIGNON PARIS REUNION 250 GRS")){
        return 'G';
    }else if(description.includes("CHAMPIGNON PARIS REUNION 500 GRS")){
        return 'H';
    }else if(description.includes("CHAMPIGNON PARIS REUNION")){
        return 'F';
    }else if(description.includes("CHAMPIGNONS BRUN CAT2 250 GRS")){
        return 'J';
    }else if(description.includes("CHAMPIGNONS BRUN CAT2 500 GRS")){
        return 'K';
    }else if(description.includes("CHAMPIGNON")){
        return 'I';
    }else{
        return -1;
    }
}

module.exports = {getOrderFromTxt: getOrderFromTxt, getOrderFromPdf:getOrderFromPdf};