const Commande = require('./commande.class');
const pdfToTxt = require('./pdfToTxt');

function getOrderFromPdf(file){
    return new Promise(async (resolve, reject) => {
        let order = null;

        pdfToTxt.exec('./newOrder/' + file)
        .then((text) => {
            if(text.includes("Saint-Paul Savanna")){
                order = new Commande("RUN MARKET SAVANNAH");
                order.rowNavette = 28;
            }else if(text.includes("Chaudron")){
                order = new Commande("RUN MARKET CHAUDRON");
                order.rowNavette = 27;
            }else if(text.includes("Saint-André")){
                order = new Commande("RUN MARKET ST ANDRE");
                order.rowNavette = 29;
            }else if(text.includes("Sainte-Marie Duparc")){
                order = new Commande("RUN MARKET STE MARIE");
                order.rowNavette = 30;
            }else{
                reject("Client n'a pas été identifier");
            }

            order.dateCommande = text[text.indexOf('Date de commande') + 1].split("/");
            order.dateCommande[2] = Math.floor((order.dateCommande[2] / 100 - Math.floor(order.dateCommande[2] / 100)) * 100);
            order.dateCommande = order.dateCommande.join("-");

            let posReading = text.indexOf("TVA") + 1;
            let commandes = [];
    
            while(!text[posReading].includes("Nb de lignes")){
                let oneCommande = {};
                let nextProduct = 9;
    
                oneCommande.ref = text[posReading];
                oneCommande.desc = text[posReading + 3];
                
    
                if(isNaN(text[posReading + 4])){
                    oneCommande.quantity = text[posReading + 5];
                    nextProduct += 1;
                }else{
                    oneCommande.quantity = text[posReading + 4];
                }
    
                let colNavette = tabCorrespondanceDescription(oneCommande.desc);
    
                if(colNavette == -1){
                    reject("Le produit n'a pas été identitifier: " + oneCommande.desc);
                }
    
                oneCommande.colNavette = colNavette;
                commandes.push(oneCommande);
                posReading += nextProduct;
            }

            let numCommande = null;

            numCommande = text[text.indexOf("N° commande") + 3];
            order.setNumCommande(numCommande);
            order.setCommande(commandes)

            resolve(order);
        })
    });
}

function tabCorrespondanceDescription(description){
    if(description.includes("ENDICES VRAC") || description.includes("ENDIVES REUNION")){
        return 'C';
    }else if(description.includes("ENDIVES SACHET 500G") || description.includes("ENDIVE SACHET 500G")){
        return 'D';
    }else if(description.includes("ENDIVES SACHET 750G")){
        return 'E';
    }else if(description.includes("CHAMP DE PARIS BL VRC 3KG") || description.includes("CHAMPIGNON DE PARIS REUNION")){
        return 'F';
    }else if(description.includes("CHAMPIGNON PARIS REUNION 250")){
        return 'G';
    }else if(description.includes("CHAMPIGNON PARIS REUNION 500")){
        return 'H';
    }else if(description.includes("CHAMP DE PARIS BR VRC 3KG") || description.includes("CHAMPIGNON BRUN")){
        return 'I';
    }else if(description.includes("CHAMPIGNONS BRUN CAT2 250")){
        return 'J';
    }else if(description.includes("CHAMPIGNONS BRUN CAT2 500")){
        return 'K';
    }else{
        return -1;
    }
}

module.exports = {getOrderFromPdf: getOrderFromPdf};