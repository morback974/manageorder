class commande{
    constructor(client){
        this.client = client;
    }

    setCommande(commandes){
        this.commandes = commandes;
    }

    setNumCommande(numCommande){
        this.numCommande = numCommande;
    }

    getNumCommande(){
        return this.numCommande.toString();
    }
}

module.exports = commande;