var pdfjsLib = require("pdfjs-dist/es5/build/pdf.js");

async function exec(path) {
    let doc = await pdfjsLib.getDocument(path).promise;
    let page1 = await doc.getPage(1);
    let content = await page1.getTextContent();
    let strings = content.items.map(function(item) {
        return item.str;
    });
    return strings;
}

module.exports = {exec: exec};