const { Console } = require('console');
const fs = require('fs');

let carrefour = require('./carrefour');
let runMarket = require('./runMarket');

let files = fs.readdirSync("newOrder");


let listOrder = [];

if(files.length == 0){
    console.log("Pas de bon de commande present");
}else{
    gestionFiles(files);
}

async function suppFichierGit(files){
    let listFiles = [];
    return new Promise((resolve, reject) => {
        files.forEach((element) => {
            if(element.split(".")[0] != ""){
                listFiles.push(element);
            }
        })

        resolve(listFiles);
    })
}


async function gestionFiles(files){
    files = await suppFichierGit(files);

    await new Promise(async (resolve, reject) => {
        for (let index = 0; index < files.length; index++) {
            const element = files[index];
            if(element.includes("EBONCMDE")){
                await carrefour.getOrderFromTxt(element)
                .then((order) => {
                    listOrder.push(order);
                    move('./newOrder/' + element, './oldOrder/' + element);
                })
                .catch((err) => {
                    console.log("Le bon de commande " + element + " n'a pas pu être traité.");
                    console.log("Details:" + err);
                })
            }else if(element.includes("Commande-")){
                await carrefour.getOrderFromPdf(element)
                .then((order) => {
                    listOrder.push(order);
                    move('./newOrder/' + element, './oldOrder/' + element);
                })
                .catch((err) => {
                    console.log("Le bon de commande " + element + " n'a pas pu être traité.");
                    console.log("Details:" + err);
                });
            }else if(element.includes("CDE_")){
                await runMarket.getOrderFromPdf(element)
                .then((order) => {
                    listOrder.push(order);
                    move('./newOrder/' + element, './oldOrder/' + element);
                })
                .catch((err) => {
                    console.log("Le bon de commande " + element + " n'a pas pu être traité.");
                    console.log("Details:" + err);
                });
            }else{
                console.log("Le fichier ('" + element + "') n'a pas été traité");
            }
        }

        resolve();
    })
    .then(() => {
        listOrder.forEach((element) =>{
            if(element.dateCommande){
                if(fs.existsSync("ficheNavette/" + element.dateCommande + ".xls")){
                    var XLSX = require('xlsx');
                    var wb = XLSX.readFile("ficheNavette/" + element.dateCommande + ".xls");
                    
                    change_workbook(wb, element); // ***
                    
                    XLSX.writeFile(wb, "ficheNavette/" + element.dateCommande + ".xls");
                }else{
                    var XLSX = require('xlsx');
                    var wb = XLSX.readFile("newNavette.xls");
                    
                    change_workbook(wb, element); // ***
                    
                    XLSX.writeFile(wb, "ficheNavette/" + element.dateCommande + ".xls");
                }
            }else{
                var XLSX = require('xlsx');
                var wb = XLSX.readFile("newNavette.xls");
                
                change_workbook(wb, element); // ***
                
                XLSX.writeFile(wb, "ficheNavette/new.xls");
            }
        });

    })
}

function change_workbook(wb, element) {
    // get the first worksheet
    var ws_name = wb.SheetNames[0];
    var ws = wb.Sheets[ws_name];

    ws["M" + element.rowNavette] = {
        t: 's', // <-- t: 's' indicates the cell is a text cell
        v: element.getNumCommande() // <-- v holds the value
    };

    element.commandes.forEach((commande) => {
        ws[commande['colNavette'] + element.rowNavette] = {
            t: 's', // <-- t: 's' indicates the cell is a text cell
            v: commande.quantity // <-- v holds the value
            };
    })
  }

function move(oldPath, newPath, needUnlink = true) {

    fs.rename(oldPath, newPath, function (err) {
        if (err) {
            if (err.code === 'EXDEV') {
                copy(needUnlink);
            } else {
                console.log(err);
            }
            return;
        }
    });

    function copy(needUnlink) {
        var readStream = fs.createReadStream(oldPath);
        var writeStream = fs.createWriteStream(newPath);

        readStream.on('error', callback);
        writeStream.on('error', callback);

        readStream.on('close', function () {
            if(needUnlink){
                fs.unlink(oldPath, callback);
            }
        });

        readStream.pipe(writeStream);
    }
}